import { Injectable } from '@angular/core';
import { comentariosI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class ComentariosService {

  listaComentarios!: comentariosI[];
  constructor() {

    if( this.obtenerLocalStorage() === undefined){
      this.listaComentarios = []
    } else {
      let ListCommets = JSON.parse(localStorage.getItem("Comentarios") || '')
      this.listaComentarios = ListCommets
    }
    
   }


  agregarNew(comentario: comentariosI){
    console.log(comentario);

    if(localStorage.getItem('Comentarios') === null){
      this.listaComentarios = [];
      this.listaComentarios.unshift(comentario);
      localStorage.setItem('Comentarios', JSON.stringify(this.listaComentarios))
    } else {
      this.listaComentarios = JSON.parse(localStorage.getItem('Comentarios') || '');
      this.listaComentarios.unshift(comentario);
      localStorage.setItem('Comentarios', JSON.stringify(this.listaComentarios))
    }

    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }

  obtenerLocalStorage(){
    // if(JSON.parse(localStorage.getItem("Comentarios") || '') === null){
    //   console.log('no comments');
    //   this.listaComentarios=[]
    //   return []
    // } else {
    //   let listaComentario = JSON.parse(localStorage.getItem("Comentarios") || '');
    //   console.log(listaComentario);
  
    //   this.listaComentarios = listaComentario
    //   // console.log(this.listaForos, 'obtener LocalStorage');
    //   return listaComentario

    // }
  }


}
