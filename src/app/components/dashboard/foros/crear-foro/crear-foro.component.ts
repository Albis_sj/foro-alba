import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Editor, Toolbar, Validators } from 'ngx-editor';
import { foroI } from 'src/app/interfaces/usuario.interface';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-crear-foro',
  templateUrl: './crear-foro.component.html',
  styleUrls: ['./crear-foro.component.css']
})
export class CrearForoComponent implements OnInit {
  
  username!: string;


  seccion: any[] = ['Grupos Masculinos', 'Grupos Femeninos'];

  Foro!: foroI;
  foro = {
    titulo: ''
  }

  html = '<b>Redacte su foro</b>';
  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ]


  constructor(private _usuariosSvc : UsuariosService,
              private _foroSvc : ForosService,
              private fb: FormBuilder) {
    this.editor = new Editor(); //se instancia el editor


    let foroID = Math.floor(Math.random() * 20)
    console.log(foroID);
    
    let nombre = this._usuariosSvc.mostrarUser();
    console.log(nombre);

    let listaForo : foroI[] = this._usuariosSvc.obtenerLocalStorage()
    console.log(listaForo);
    let coincidencia = listaForo.find(e => e.foroid !== foroID)
    console.log(coincidencia);
    
   }

  ngOnInit(): void {
  }

  guardar(){
    let fecha = new Date();

    let foroID = Math.floor(Math.random() * 1000)
    console.log(foroID);
    
    let nombre = this._usuariosSvc.mostrarUser();
    let listaForo : foroI[] = this._usuariosSvc.obtenerLocalStorage()
    console.log(listaForo);

    while (listaForo.find(e => e.foroid === foroID)){
      console.log('es igual');
      foroID = Math.floor(Math.random() * 20)
    }
    
    if(nombre === null){
      console.log('no se pudo');
    } else {
      console.log(foroID);
      
      this.Foro = {
        foroid: foroID,
        usuario: nombre,
        seccion: 0,
        titulo: this.foro.titulo,
        fecha: fecha.toLocaleDateString(),
        foro: this.html
      };
      console.log(this.Foro);

    } 

    // this.subirAlLocal();
  }

  subirAlLocal(){
    this._foroSvc.agregarNew(this.Foro)
  }

  traerUsuario(){
   let nombre = this._usuariosSvc.mostrarUser()
  return nombre
  }

  // verForo(number: number){
  //   this.html = this._foroSvc.obtenerLocalStorage()
  //   console.log('Mostro????');
  //   console.log(this.html);
  // }

}