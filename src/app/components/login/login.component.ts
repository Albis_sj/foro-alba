import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from 'ngx-editor';
import { UsuarioI, UsuarioRegistradoI } from 'src/app/interfaces/usuario.interface';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  loading: boolean = false;
  lista = [];

  constructor(private fb: FormBuilder,
              private router: Router,
              private _usuariosService: UsuariosService ) { 
    this.formulario();

    this.lista = this._usuariosService.obtenerLocalStorage();
    console.log(this.lista);
    
  }

  ngOnInit(): void {
  }

  formulario(): void {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  Ingresar():void{
    console.log(this.form.value);

    const Usuario: UsuarioI = {
      usuario: this.form.value.usuario,
      password: this.form.value.password
    }

    console.log(Usuario);

    //Conserva el nombre
    this._usuariosService.loginDatos(Usuario)
    
    
    const lista: UsuarioRegistradoI[] = JSON.parse(localStorage.getItem("Citas") || '');
    console.log(lista);
    
  // const encontraado = lista.find(e => e.usuario === Usuario.usuario) && lista.find(e => e.contraseña1 === Usuario.password);
  // console.log(lista.find(e => e.usuario === 'Albis'), 'log encontrado');
  // console.log(Usuario.password);
  
  // console.log(lista.find(e => e.contraseña2 === 'KNJKSJMYG'));
  // console.log(lista[0]);
  
  
  // console.log(encontraado);
  

    if(lista.find(e => e.usuario === Usuario.usuario)){
      //Redireccionaremos al dashboard
      console.log('Usuario Logeado');
      
      this.fakeLoading();
    }else{
      //Mostramos un mensaje de error
      this._usuariosService.snackbar(1);
      this.form.reset();
    }
  }

  fakeLoading(){
    this.loading = true
    setTimeout(() => {
      this.loading = false;

      //Redireccionamos al dashboard
      this.router.navigate(['dashboard']);
    }, 1500);
  }

}
